/**
 * Connor Muray
 * 991553779
 */
package validator;

import static org.junit.Assert.*;

import org.junit.Test;



public class PasswordValidatorTest {
	@Test
	public void testpassUppercase() {
		assertFalse("Password must contain a uppercase",
		PasswordValidator.passUppercase("Sheridan!!") == false);
	}
	@Test
	public void testpassUppercaseException() {
		assertTrue("Password must contain at least one uppercase",
		PasswordValidator.passUppercase("sheri!!!!!!@#") == false);
	}
	@Test
	public void testpassUppercaseBoundryIn() {
		assertTrue("Password must be a complete word",
		PasswordValidator.passUppercase("Sh--eridan") == true);
	}
	@Test
	public void testpassUppercaseBoundryOut() {
		assertFalse("Password must have one uppercase",
		PasswordValidator.passUppercase("Sheridan") == false);
	}
	

	
	/*
	 * @Test public void testpassHasNum() { assertTrue("Password needs two digits",
	 * PasswordValidator.passHasNum("Sheridan22") == true); }
	 * 
	 * @Test public void testpassHasNumException() {
	 * assertTrue("Password MUST have two digits!",
	 * PasswordValidator.passHasNum("Sheridan22") == true); }
	 * 
	 * @Test public void testpassHasNumBoundryIn() {
	 * assertTrue("Password MUST have two digits!",
	 * PasswordValidator.passHasNum("S22#@#$!") == true); }
	 * 
	 * @Test public void testpassHasNumBoundryOut() {
	 * assertTrue("Password MUST have two digits!",
	 * PasswordValidator.passHasNum("  22   ") == true); }
	 */
	
	
	
	
//	@Test
//	public void testpassHasNum() {
//		assertTrue("Password isn't the correct length!", PasswordValidator.passHasNum("SHeridanCOllege321") == true);
//	}
//	@Test
//	public void testpassLengthException() {
//		assertTrue("Password isn't the correct length!", 
//				PasswordValidator.passLength("SheridanCollege") == true);
//	}
//	@Test 
//	public void testpassLength() {
//		assertTrue("Password isn't the correct length!",
//				PasswordValidator.passLength("SHeridanCOllege") == true); 
//	}
//	@Test
//	public void testpassLengthBoundryIn() { 
//		assertTrue("Password isn't the correct length",
//				PasswordValidator.passLength("Shi         ") == true);
//	}
//	@Test
//	public void testpassLengthBoundryOut() { 
//		assertTrue("Password isn't the correct length",
//				PasswordValidator.passLength("SheridanCollege") == true);
//	}
}
